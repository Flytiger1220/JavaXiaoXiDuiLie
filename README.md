# Java消息队列ActiveMQ+jms+springboot demo

Java消息队列学习demo，包含三个test。分别是：

1、queue和双向queue消息发送

2、topic消息发送

3、queue和topic同时发送


其中单独实现topic时，需要在application.properties文件中加入：

spring.jms.pub-sub-domain=true


可以通过以下链接

http://127.0.0.1:8161/admin/topics.jsp
获取消息发送情况



