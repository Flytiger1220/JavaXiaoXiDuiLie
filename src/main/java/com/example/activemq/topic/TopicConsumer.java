package com.example.activemq.topic;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class TopicConsumer {
    /*
    * 两个receiveTopic测试topic模式，两个消费者同时接收消息的情况
    * */
    // 使用JmsListener配置消费者监听的队列，其中text是接收到的消息
    @JmsListener(destination = "mytest.topic")
    public void receiveTopic(String text) {
        System.out.println("TopicConsumer收到的topic报文为:"+text);
    }
    @JmsListener(destination = "mytest.topic")
    public void receiveTopic1(String text) {
        System.out.println("TopicConsumer1收到的topic报文为:"+text);
    }
}
