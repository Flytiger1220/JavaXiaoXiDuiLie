package com.example.activemq.queue;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
public class QueueConsumer2 {
    // 使用JmsListener配置消费者监听的队列，其中text是接收到的消息
    @JmsListener(destination = "mytest.queue")
    //SendTo 该注解的意思是将return回的值，再发送的"out.queue"队列中
    @SendTo("out.queue")
    public String receiveQueue(String text) {
        System.out.println("QueueConsumer2收到的报文为:"+text);
        return "return message "+text;
    }
}
