package com.example.activemq.all;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    private final static Logger logger = LoggerFactory.getLogger(Consumer.class);

    @JmsListener(destination = JmsConfig.TOPIC, containerFactory = "jmsListenerContainerTopic")
    public void onTopicMessage(String msg) {
        logger.info("Consumer1接收到topic消息：{}",msg);
    }
    @JmsListener(destination = JmsConfig.TOPIC, containerFactory = "jmsListenerContainerTopic")
    public void onTopicMessage2(String msg) {
        logger.info("Consumer2接收到topic消息：{}",msg);
    }
    @JmsListener(destination = JmsConfig.QUEUE, containerFactory = "jmsListenerContainerQueue")
    public void onQueueMessage(String msg) {
        logger.info("Consumer接收到queue消息：{}",msg);
    }
}
