package com.example.activemq;

import com.example.activemq.all.Producer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.Queue;
import javax.jms.Topic;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivemqQueueAndTopicTests{
	@Autowired
	private Producer producer;
	@Autowired
	private Topic topic;
	@Autowired
	private Queue queue;

	@Test
	public void testJms() {
		for (int i=0;i<3;i++) {
			producer.sendMessage(queue,"queue,world!" + i);
			producer.sendMessage(topic, "topic,world!" + i);
		}
	}
}

